import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";

const prisma = new PrismaClient();

const categoriesCount = 5; // Number of categories to create
const collectionsCount = 5; // Number of collections to create

const productsCount = 5; // Number of products to create

await prisma.category.deleteMany();
for (let i = 0; i < categoriesCount; i++) {
	await prisma.category.create({
		data: {
			name: faker.commerce.productMaterial(),
		},
	});
}

await prisma.collection.deleteMany();
for (let i = 0; i < collectionsCount; i++) {
	await prisma.collection.create({
		data: {
			name: faker.commerce.productAdjective(),
		},
	});
}

await prisma.product.deleteMany();
const categories = await prisma.category.findMany();
const collections = await prisma.collection.findMany();
function randomIntFromInterval(min: number, max: number) {
	// min and max included
	return Math.floor(Math.random() * (max - min + 1) + min);
}

for (let i = 0; i < productsCount; i++) {
	const name = faker.commerce.productName();
	const category =
		categories[randomIntFromInterval(0, categories.length - 1)];
	const collection =
		collections[randomIntFromInterval(0, collections.length - 1)];
	await prisma.product.create({
		data: {
			name: name,
			slug: faker.helpers.slugify(name).toLowerCase(),
			description: faker.commerce.productDescription(),
			price: Number(faker.commerce.price()) * 100,
			images: [faker.image.url()],
			categories: {
				connect: {
					id: category.id,
				},
			},
			collections: {
				connect: {
					id: collection.id,
				},
			},
		},
	});
}
console.log("Products created");
