import type { DocumentNode } from "graphql";
export const typeDefs = {
	kind: "Document",
	definitions: [
		{
			kind: "ObjectTypeExtension",
			name: {
				kind: "Name",
				value: "Query",
				loc: { start: 12, end: 17 },
			},
			interfaces: [],
			directives: [],
			fields: [
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "product",
						loc: { start: 22, end: 29 },
					},
					arguments: [
						{
							kind: "InputValueDefinition",
							name: {
								kind: "Name",
								value: "id",
								loc: { start: 30, end: 32 },
							},
							type: {
								kind: "NonNullType",
								type: {
									kind: "NamedType",
									name: {
										kind: "Name",
										value: "ID",
										loc: { start: 34, end: 36 },
									},
									loc: { start: 34, end: 36 },
								},
								loc: { start: 34, end: 37 },
							},
							directives: [],
							loc: { start: 30, end: 37 },
						},
					],
					type: {
						kind: "NamedType",
						name: {
							kind: "Name",
							value: "Product",
							loc: { start: 40, end: 47 },
						},
						loc: { start: 40, end: 47 },
					},
					directives: [],
					loc: { start: 22, end: 47 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "products",
						loc: { start: 50, end: 58 },
					},
					arguments: [
						{
							kind: "InputValueDefinition",
							name: {
								kind: "Name",
								value: "first",
								loc: { start: 59, end: 64 },
							},
							type: {
								kind: "NonNullType",
								type: {
									kind: "NamedType",
									name: {
										kind: "Name",
										value: "Int",
										loc: { start: 66, end: 69 },
									},
									loc: { start: 66, end: 69 },
								},
								loc: { start: 66, end: 70 },
							},
							directives: [],
							loc: { start: 59, end: 70 },
						},
						{
							kind: "InputValueDefinition",
							name: {
								kind: "Name",
								value: "skip",
								loc: { start: 72, end: 76 },
							},
							type: {
								kind: "NamedType",
								name: {
									kind: "Name",
									value: "Int",
									loc: { start: 78, end: 81 },
								},
								loc: { start: 78, end: 81 },
							},
							directives: [],
							loc: { start: 72, end: 81 },
						},
					],
					type: {
						kind: "NonNullType",
						type: {
							kind: "ListType",
							type: {
								kind: "NonNullType",
								type: {
									kind: "NamedType",
									name: {
										kind: "Name",
										value: "Product",
										loc: { start: 85, end: 92 },
									},
									loc: { start: 85, end: 92 },
								},
								loc: { start: 85, end: 93 },
							},
							loc: { start: 84, end: 94 },
						},
						loc: { start: 84, end: 95 },
					},
					directives: [],
					loc: { start: 50, end: 95 },
				},
			],
			loc: { start: 0, end: 97 },
		},
		{
			kind: "ObjectTypeDefinition",
			name: {
				kind: "Name",
				value: "Image",
				loc: { start: 104, end: 109 },
			},
			interfaces: [],
			directives: [],
			fields: [
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "url",
						loc: { start: 114, end: 117 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 119, end: 125 },
							},
							loc: { start: 119, end: 125 },
						},
						loc: { start: 119, end: 126 },
					},
					directives: [],
					loc: { start: 114, end: 126 },
				},
			],
			loc: { start: 99, end: 128 },
		},
		{
			kind: "ObjectTypeDefinition",
			name: {
				kind: "Name",
				value: "Product",
				loc: { start: 135, end: 142 },
			},
			interfaces: [],
			directives: [],
			fields: [
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "id",
						loc: { start: 147, end: 149 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "ID",
								loc: { start: 151, end: 153 },
							},
							loc: { start: 151, end: 153 },
						},
						loc: { start: 151, end: 154 },
					},
					directives: [],
					loc: { start: 147, end: 154 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "name",
						loc: { start: 157, end: 161 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 163, end: 169 },
							},
							loc: { start: 163, end: 169 },
						},
						loc: { start: 163, end: 170 },
					},
					directives: [],
					loc: { start: 157, end: 170 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "slug",
						loc: { start: 173, end: 177 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 179, end: 185 },
							},
							loc: { start: 179, end: 185 },
						},
						loc: { start: 179, end: 186 },
					},
					directives: [],
					loc: { start: 173, end: 186 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "description",
						loc: { start: 189, end: 200 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 202, end: 208 },
							},
							loc: { start: 202, end: 208 },
						},
						loc: { start: 202, end: 209 },
					},
					directives: [],
					loc: { start: 189, end: 209 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "price",
						loc: { start: 212, end: 217 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "Int",
								loc: { start: 219, end: 222 },
							},
							loc: { start: 219, end: 222 },
						},
						loc: { start: 219, end: 223 },
					},
					directives: [],
					loc: { start: 212, end: 223 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "images",
						loc: { start: 226, end: 232 },
					},
					arguments: [],
					type: {
						kind: "ListType",
						type: {
							kind: "NonNullType",
							type: {
								kind: "NamedType",
								name: {
									kind: "Name",
									value: "Image",
									loc: { start: 235, end: 240 },
								},
								loc: { start: 235, end: 240 },
							},
							loc: { start: 235, end: 241 },
						},
						loc: { start: 234, end: 242 },
					},
					directives: [],
					loc: { start: 226, end: 242 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "reviews",
						loc: { start: 245, end: 252 },
					},
					arguments: [],
					type: {
						kind: "ListType",
						type: {
							kind: "NonNullType",
							type: {
								kind: "NamedType",
								name: {
									kind: "Name",
									value: "Review",
									loc: { start: 255, end: 261 },
								},
								loc: { start: 255, end: 261 },
							},
							loc: { start: 255, end: 262 },
						},
						loc: { start: 254, end: 263 },
					},
					directives: [],
					loc: { start: 245, end: 263 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "createdAt",
						loc: { start: 266, end: 275 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "DateTime",
								loc: { start: 277, end: 285 },
							},
							loc: { start: 277, end: 285 },
						},
						loc: { start: 277, end: 286 },
					},
					directives: [],
					loc: { start: 266, end: 286 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "updatedAt",
						loc: { start: 289, end: 298 },
					},
					arguments: [],
					type: {
						kind: "NamedType",
						name: {
							kind: "Name",
							value: "DateTime",
							loc: { start: 300, end: 308 },
						},
						loc: { start: 300, end: 308 },
					},
					directives: [],
					loc: { start: 289, end: 308 },
				},
			],
			loc: { start: 130, end: 310 },
		},
		{
			kind: "ObjectTypeDefinition",
			name: {
				kind: "Name",
				value: "Review",
				loc: { start: 316, end: 322 },
			},
			interfaces: [],
			directives: [],
			fields: [
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "id",
						loc: { start: 327, end: 329 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "ID",
								loc: { start: 331, end: 333 },
							},
							loc: { start: 331, end: 333 },
						},
						loc: { start: 331, end: 334 },
					},
					directives: [],
					loc: { start: 327, end: 334 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "title",
						loc: { start: 337, end: 342 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 344, end: 350 },
							},
							loc: { start: 344, end: 350 },
						},
						loc: { start: 344, end: 351 },
					},
					directives: [],
					loc: { start: 337, end: 351 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "description",
						loc: { start: 354, end: 365 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 367, end: 373 },
							},
							loc: { start: 367, end: 373 },
						},
						loc: { start: 367, end: 374 },
					},
					directives: [],
					loc: { start: 354, end: 374 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "rating",
						loc: { start: 377, end: 383 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "Int",
								loc: { start: 385, end: 388 },
							},
							loc: { start: 385, end: 388 },
						},
						loc: { start: 385, end: 389 },
					},
					directives: [],
					loc: { start: 377, end: 389 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "productId",
						loc: { start: 392, end: 401 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "String",
								loc: { start: 403, end: 409 },
							},
							loc: { start: 403, end: 409 },
						},
						loc: { start: 403, end: 410 },
					},
					directives: [],
					loc: { start: 392, end: 410 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "createdAt",
						loc: { start: 413, end: 422 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "DateTime",
								loc: { start: 424, end: 432 },
							},
							loc: { start: 424, end: 432 },
						},
						loc: { start: 424, end: 433 },
					},
					directives: [],
					loc: { start: 413, end: 433 },
				},
				{
					kind: "FieldDefinition",
					name: {
						kind: "Name",
						value: "updatedAt",
						loc: { start: 436, end: 445 },
					},
					arguments: [],
					type: {
						kind: "NonNullType",
						type: {
							kind: "NamedType",
							name: {
								kind: "Name",
								value: "DateTime",
								loc: { start: 447, end: 455 },
							},
							loc: { start: 447, end: 455 },
						},
						loc: { start: 447, end: 456 },
					},
					directives: [],
					loc: { start: 436, end: 456 },
				},
			],
			loc: { start: 311, end: 458 },
		},
		{
			kind: "ObjectTypeDefinition",
			name: {
				kind: "Name",
				value: "Query",
				loc: { start: 464, end: 469 },
			},
			interfaces: [],
			directives: [],
			fields: [],
			loc: { start: 459, end: 469 },
		},
		{
			kind: "ScalarTypeDefinition",
			name: {
				kind: "Name",
				value: "DateTime",
				loc: { start: 478, end: 486 },
			},
			directives: [],
			loc: { start: 471, end: 486 },
		},
	],
	loc: { start: 0, end: 487 },
} as unknown as DocumentNode;
