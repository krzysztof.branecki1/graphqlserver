import type { QueryResolvers } from "./../../../types.generated";
export const product: NonNullable<QueryResolvers["product"]> = async (
	_parent,
	arg,
	ctx,
) => {
	const product = await ctx.prisma.product.findUnique({
		where: { id: arg.id },
	});
	if (!product) {
		return null;
	}
	return {
		id: product.id,
		slug: product.slug,
		name: product.name,
		description: product.description,
		price: product.price,
		updatedAt: product.updatedAt,
		createdAt: product.createdAt,
		images: [],
		reviews: [],
	};
};
